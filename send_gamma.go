package gamma_client

import (
	"bitbucket.org/snapmartinc/gamma-client/models"
	"encoding/json"
)

func (c *client) SendGamma(input *models.SendGammaInput) (m *models.SendGammaResponse, err error) {
	data, err := c.send("POST", "/cgphttp/servlet/sendott", input)
	if err != nil {
		return nil, err
	}
	if err = json.Unmarshal(data, m); err != nil {
		return nil, err
	}
	return m, nil
}
