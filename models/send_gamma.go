package models

type SendGammaInput struct {
	Destination string `json:"destination"`
	Text        string `json:"text"`
}

type SendGammaResponse struct {
	SuccessCode    string `json:"success_code"`
	ResponseCode   string `json:"response_code"`
	ResponseString string `json:"response_string"`
	MessageID      string `json:"message_id"`
}
