package gamma_client

import (
	"bitbucket.org/snapmartinc/gamma-client/models"
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

type Client interface {
	SendGamma(input *models.SendGammaInput) (*models.SendGammaResponse, error)
}

type client struct {
	Host       string
	Credential Credential
	httpClient http.Client
}

func New(host string, httpClient http.Client, credential Credential) *client {
	return &client{
		Host:       host,
		httpClient: httpClient,
		Credential: credential,
	}
}

type Credential struct {
	Username string
	Password string
}

func (c client) send(method string, uri string, input interface{}) ([]byte, error) {
	body, err := json.Marshal(input)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(
		method,
		c.Host+uri,
		bytes.NewBuffer(body),
	)

	if err != nil {
		return nil, err
	}
	req.SetBasicAuth(c.Credential.Username, c.Credential.Password)

	res, err := c.httpClient.Do(req)

	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	return ioutil.ReadAll(res.Body)
}
